clear all;
close all;

load link_cap_ul.txt
time = link_cap_ul(:,1);
cap = link_cap_ul(:,2);

s_cap = EWMA(cap,0.5);

plot(time,s_cap,g'-');
