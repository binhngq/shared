clear all;
close all;

load link_cap_ul.dat
time = link_cap_ul(:,1);
cap = link_cap_ul(:,2);

%s_cap = EWMA(cap,0.5);

plot(time/1000,smoothed_z(cap,0.5),'o');

xlabel('time stamp (s)');
ylabel('link capacity (Mbps)');
title('Link capacity');

h=figure;
saveas(h,'link-cap','png');
